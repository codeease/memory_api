import express from 'express';
import user from './api/user.js';
import login from './api/login.js';
import subject from './api/subject.js';
import parent from './api/parent.js';
import staff from './api/staff.js';

const server = express();
const port = process.env.PORT || 4000;

server.use('/', user);
server.use('/', login);
server.use('/', subject);
server.use('/', parent);
server.use('/', staff);

server.listen(port, () => {
  console.log('Starting server on port', port);
});

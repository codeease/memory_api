import app from '../myexpress.js';

const company = app.get('api');
const mysql = app.get('connection');

company.get('/company', (req, res) => {
  const query = mysql.query('select * from company', (err, result) => {
    console.log(query.sql);
    if (err) {
      console.error(err);
      return;
    }
    res.send(result);
  });
});

company.get('/company/:id', (req, res) => {
  const query = mysql.query(
    'select * from company where company_id = ?',
    req.params.id,
    (err, result) => {
      console.log(query.sql);
      if (err) {
        console.error(err);
        return;
      }
      res.send(result);
    });
});

company.get('/company/search/:searchValue', (req, res) => {
  const query = mysql.query(
    'select * from company where company_name like ? OR company_contact like ?',
    [`%${req.params.searchValue}%`, `%${req.params.searchValue}%`],
    (err, result) => {
      console.log(query.sql);
      if (err) {
        console.log(err);
        return;
      }
      res.send(result);
    }
  );
});

company.post('/company', (req, res) => {
  const query = mysql.query('insert into company set ?', req.body, (err, result) => {
    console.log(query.sql);
    if (err) {
      console.error(err);
      return;
    }
    res.send(result);
  });
});

company.put('/company/:companyId', (req, res) => {
  const query = mysql.query(
    'update company set ? where company_id = ?',
    [req.body, req.params.companyId],
    (err, result) => {
      console.log(query.sql);
      if (err) {
        console.error(err);
        return;
      }
      res.send(result);
    }
  );
});

company.delete('/company/:companyId', (req, res) => {
  const query = mysql.query(
    'delete from company where company_id = ?',
    req.params.companyId,
    (err, result) => {
      console.log(query.sql);
      if (err) {
        console.log(err);
        return;
      }
      res.send(result);
    });
});

export default company;

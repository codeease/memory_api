import app from '../myexpress.js';

const subject = app.get('api');
const mysql = app.get('connection');

//Get all the subjects
subject.get('/subject', (req, res) => {
    const query = mysql.query('select * from subject', (err, result) => {
        console.log(query.sql);
        if (err) {
            console.error(err);
            return;
        }
        res.send(result);
    });
});

//Get subject based on subject id
subject.get('/subject/:subjectId', (req, res) => {
    const query = mysql.query(
        'select * from subject where subject_id = ?',
        req.params.subjectId,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});

//Get or search subject based on subject code
subject.get('/subject/search/:subjectCode', (req, res) => {
    const query = mysql.query(
        'select * from subject where subject_code = ?',
        req.params.subjectCode,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});

//Add new subject
subject.post('/subject', (req, res) => {
    const query = mysql.query('insert into subject set ?', req.body, (err, result) => {
        console.log(query.sql);
        if (err) {
            console.error(err);
            return;
        }
        res.send(result);
    });
});

//Modify subject details based on subject id
subject.put('/subject/:subjectId', (req, res) => {
    const query = mysql.query(
        'update subject set ? where subject_id = ?',
        [req.body, req.params.subjectId],
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.error(err);
                return;
            }
            res.send(result);
        }
    );
});

//Delete subject based on subject code
subject.delete('/subject/:subjectCode', (req, res) => {
    const query = mysql.query(
        'delete from subject where subject_code= ?',
        req.params.subjectCode,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});
/*
var subject = {
    subject_code: 'eng102',
    subject_name: 'English Language 2',
    subject_about: 'steps to English language. This covers areas such as identifying verbs, nouns etc.'
};


 //Add new subject to database
connection.query('insert into subject set ?', subject, function( err, result){
    if(err){
        console.error(err);
        return;
    }
    console.error(result);
});

 //Get all the subjects
connection.query('select * from subject', function(err, result){
    console.log(result);
});

//Get single subject based on the subject code
var subjectCode = 'mth101';
connection.query('select * from subject where subject_code =' + connection.escape(subjectCode), function(err, result){
    console.log(result);
});*/

export default subject;
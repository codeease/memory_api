import app from '../myexpress.js';

const parent = app.get('api');
const mysql = app.get('connection');

//Get all the parents
parent.get('/parent', (req, res) => {
    const query = mysql.query('select * from parent', (err, result) => {
        console.log(query.sql);
        if (err) {
            console.error(err);
            return;
        }
        res.send(result);
    });
});

//Get parent based on parent id
parent.get('/parent/:parentId', (req, res) => {
    const query = mysql.query(
        'select * from parent where parent_id = ?',
        req.params.parentId,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});

//Get or search parent based on parent name
parent.get('/parent/search/:parentName', (req, res) => {
    const query = mysql.query(
        'select * from parent where parent_name = ?',
        req.params.parentName,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});

//Add new parent
parent.post('/parent', (req, res) => {
    const query = mysql.query('insert into parent set ?', req.body, (err, result) => {
        console.log(query.sql);
        if (err) {
            console.error(err);
            return;
        }
        res.send(result);
    });
});

//Modify parent info based on parent id
parent.put('/parent/:parentId', (req, res) => {
    const query = mysql.query(
        'update parent set ? where parent_id = ?',
        [req.body, req.params.parentId],
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.error(err);
                return;
            }
            res.send(result);
        }
    );
});

//Delete parent based on parent name
parent.delete('/parent/:parentName', (req, res) => {
    const query = mysql.query(
        'delete from parent where parent_name= ?',
        req.params.parentName,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});

export default parent;
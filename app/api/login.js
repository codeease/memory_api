import app from '../myexpress.js';

const login = app.get('api');
const passport = app.get('passport');

login.post('/login', passport.authenticate('local'), (req, res) => {
  res.send(req.user);
});

login.get('/login', (req, res) => {
  res.send('shout out');
});

export default login;

import app from '../myexpress.js';

const staff = app.get('api');
const mysql = app.get('connection');

//Get all the staff
staff.get('/staff', (req, res) => {
    const query = mysql.query('select * from staff', (err, result) => {
        console.log(query.sql);
        if (err) {
            console.error(err);
            return;
        }
        res.send(result);
    });
});

//Get staff based on staff id
staff.get('/staff/:staffId', (req, res) => {
    const query = mysql.query(
        'select * from staff where staff_id = ?',
        req.params.staffId,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});

//Get or search staff based on staff name
staff.get('/staff/search/:staffName', (req, res) => {
    const query = mysql.query(
        'select * from staff where staff_fullname = ?',
        req.params.staffName,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});

//Add new staff
staff.post('/staff', (req, res) => {
    const query = mysql.query('insert into staff set ?', req.body, (err, result) => {
        console.log(query.sql);
        if (err) {
            console.error(err);
            return;
        }
        res.send(result);
    });
});

//Modify staff info based on staff number
staff.put('/staff/:staffNumber', (req, res) => {
    const query = mysql.query(
        'update staff set ? where staff_number = ?',
        [req.body, req.params.staffNumber],
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.error(err);
                return;
            }
            res.send(result);
        }
    );
});

//Delete staff based on staff name
staff.delete('/staff/:staffName', (req, res) => {
    const query = mysql.query(
        'delete from staff where staff_fullname= ?',
        req.params.staffName,
        (err, result) => {
            console.log(query.sql);
            if (err) {
                console.log(err);
                return;
            }
            res.send(result);
        });
});

export default staff;
import express from 'express';
import mysql from 'mysql';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import expressSession from 'express-session';
import passport from 'passport';
import passportLocal from 'passport-local';

// database connection
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'memory'
});

const api = express();

// dependency injection
api.use(bodyParser.urlencoded({ extended: true })); // for post methods
api.use(cookieParser());
api.use(expressSession({
  secret: process.env.SESSION_SECRET || 'secret',
  resave: false,
  saveUninitialized: false,
}));
api.use(passport.initialize());
api.use(passport.session());
api.use(express.static('public')); // makes public ommitted in url
api.use(bodyParser.json()); // for reading json

connection.connect(); // actual db connection

passport.use(new passportLocal.Strategy((username, password, callBack) => {
  if (username === password) {
    callBack(null, { id: username, name: username });
  } else {
    callBack(null, null);
  }
}));

passport.serializeUser((user, callBack) => {
  callBack(null, user.id);
});

passport.deserializeUser((id, callBack) => {
  callBack(null, {
    id: id,
    name: id,
  });
});

const app = new Map([
  ['api', api],
  ['connection', connection],
  ['passport', passport],
]);
export default app;
